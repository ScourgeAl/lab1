import random
import sys
import os

if __name__ == '__main__':
    res = int(sys.argv[1])
    os.makedirs("res", exist_ok = True)
    with open("res/res.txt", "w+") as f:
        f.write(str(oct(res))[2:])
        f.write("\n")
        f.write(str(hex(res))[2:])